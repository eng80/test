package org.example;

import lombok.Getter;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    final static String[] names = {"Alisa", "Bob", "Carol", "Dan", "Erik"};
    static final Random rnd = new Random();
    static final Function<Integer, String> logInt2Str = num -> {
//        try { Thread.sleep(rnd.nextInt(2)); } catch (Exception ex) {}
        System.out.println("finding the name " + names[num] + " in the thread " + Thread.currentThread().getName());
        return names[num];
    };

    @Test
    public void coldPublisherTest() {
        System.out.println();
        Flux<String> flux = Flux.range(0, 4).map(num -> names[num]);
        flux.subscribe(it -> System.out.println("first received " + it), Throwable::printStackTrace);
        flux.subscribe(it -> System.out.println("second received " + it), Throwable::printStackTrace);
        System.out.println();
    }

    @Test
    public void hotPublisherTest() throws Exception {
        System.out.println();
        Flux<String> flux = Flux.range(0, 4).map(num -> names[num]);
        // define hot publisher
        ConnectableFlux<String> cFlux = flux.publish();
        //Duration introduce parallel execution to provide time delay scheduller
        cFlux.delayElements(Duration.ofMillis(50)).subscribe(it -> System.out.println("first received " + it), Throwable::printStackTrace);
//        cFlux.subscribe(it -> System.out.println("first received " + it), Throwable::printStackTrace);
        // start emitting
        cFlux.connect();
        Thread.sleep(100);
        cFlux.subscribe(it -> System.out.println("second received " + it), Throwable::printStackTrace);
        Thread.sleep(1000);
    }

    @Test
    public void schedullersTest() throws Exception {
        Flux<String> flux = Flux.range(0, 4).map(logInt2Str);
        Scheduler s = Schedulers.boundedElastic();
        //synchronous execution
        flux.subscribe(it -> System.out.println("First received " + it + " in the Thread " + Thread.currentThread().getName()));
        //before publishOn(s) performs in the main thread, after - in the thread s
        System.out.println();
        flux.publishOn(s).subscribe(it -> System.out.println("Second received " + it + " in the Thread " + Thread.currentThread().getName()));
        Thread.sleep(50);
        System.out.println();
        //the same, but subscribe was split into doOnNext(...) ... subscribe() and publishOn(s) was inserted between them
        // -> synchronous execution is in the main thread before publishOn(s) and asynchronous after
        flux.doOnNext(it -> System.out.println("3.0th received " + it + " in the Thread " + Thread.currentThread().getName()))
                .publishOn(s).doOnNext(it -> System.out.println("3.1th received " + it + " in the Thread " + Thread.currentThread().getName()))
                .subscribe();
        System.out.println();
        //2 publishOn, 2 doOnNext work synchronously but in the different threads
        flux.doOnNext(it -> System.out.println("4.0th received " + it + " in the Thread " + Thread.currentThread().getName()))
                .publishOn(s).doOnNext(it -> {
                    try {
                         Thread.sleep(1);
                    } catch (Exception ex) {
                    }
                    ;
                    System.out.println("4.1th received " + it + " in the Thread " + Thread.currentThread().getName());
                })
                .publishOn(s).doOnNext(it -> System.out.println("4.2th received " + it + " in the Thread " + Thread.currentThread().getName()))
                .subscribe();
        System.out.println();
        //synchronous execution in the thread s because subscribeOn(s) propagates signal throughout the data flow
        flux.subscribeOn(s).doOnNext(it -> System.out.println("Fifth received " + it + " in the Thread " + Thread.currentThread().getName())).subscribe();
    }

    @Test
    public void stepVerifierTest() throws Exception {
        //there is specified 5 virtual hours of delay for each component
        StepVerifier.withVirtualTime(() -> Flux.range(0, 4).delayElements(Duration.ofHours(8)))
                //here is virtual 2-days delay after subscription
                .expectSubscription().thenAwait(Duration.ofDays(2))
                //all virtual emits have been made since now
                .expectNextCount(4)
                .verifyComplete();
    }

    @Getter
    static class Dto {
        Integer id;
        String name;
        public Dto() {
            super();
        }
        static Dto of(Integer id, String name) {
            Dto result = new Dto();
            result.id = id;
            result.name = name;
            return result;
        }
    }

    @Test
    public void test() {
        Dto[] dtos = {Dto.of(1, "a"), Dto.of(2, "b"), Dto.of(3, "a")};
        Map<String, List<Integer>> map = Stream.of(dtos).collect(Collectors.groupingBy(Dto::getName,
                Collectors.mapping(Dto::getId, Collectors.toList())));
        map.entrySet().forEach(System.out::println);
    }

}