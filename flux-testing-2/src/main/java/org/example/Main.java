package org.example;

import reactor.core.publisher.Flux;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Flux<String> flux = Flux.range(0,4).map(num -> num.toString());
        flux.subscribe(item -> System.out.println(item));
    }
}