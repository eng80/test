package org.tasks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RomanianLetters {

    public static String intToRoman(int num) {

        Map<Integer, String>[] maps = new Map[5];
        for (int ind = 0; ind < 5; ind++) {
            maps[ind] = new HashMap<>();
        }

        maps[0].put(1, "I");
        maps[0].put(5, "V");
        maps[1].put(1, "X");
        maps[1].put(5, "L");
        maps[2].put(1, "C");
        maps[2].put(5, "D");
        maps[3].put(1, "M");
        maps[3].put(5, "MMMMM");
        maps[4].put(1, "MMMMMMMMMM");

        List<Integer> digits = new ArrayList<>();
        int rest = num;
        while (rest > 0) {
            digits.add(rest % 10);
            rest /= 10;
        }

        StringBuilder sb = new StringBuilder();
        for (int ind = digits.size() - 1; ind >= 0; ind--) {
            int d = digits.get(ind);
            if (d >= 9) {
                sb.append(maps[ind].get(1) + maps[ind + 1].get(1));
                d -= 9;
            }
            if (d >= 5) {
                sb.append(maps[ind].get(5));
                d -= 5;
            }
            if (d >= 4) {
                sb.append(maps[ind].get(1) + maps[ind].get(5));
                d -= 4;
            }
            for (int ind1 = 0; ind1 < d; ind1++) {
                sb.append(maps[ind].get(1));
            }
        }

        return sb.toString();
    }
}
