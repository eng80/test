package org.tasks;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    public void romanianLetterTest() {
        assertEquals("III",RomanianLetters.intToRoman(3));
        assertEquals("LVIII",RomanianLetters.intToRoman(58));
        assertEquals("MCMXCIV",RomanianLetters.intToRoman(1994));
    }
}