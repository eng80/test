Приложение simple_scada создано и отлажено в ОС Ubuntu 18.04, СУБД PostgreSQL 10.12, Java 1.8.0_201, 
Gradle 5.0, фрэймворк Spring Boot 2.2.5.

Для работы потребуется запущенная (не встроенная) база данных 
в виде 3 таблиц, реализующих схему m:n таблиц object и sensor. 
Соединительная таблица measure содержит также логи измерений.

База строится автоматически при запуске приложения. Также ее можно создать вручную, 
для ее построения подходят следующие DDL скрипты:

create table if not exists object (id integer not null primary key);
create table if not exists sensor (id integer not null primary key);
create table if not exists measure (oid integer not null, sid integer not null, timestamp integer not null, value float not null);
alter table measure drop constraint if exists fk_object;
alter table measure add constraint fk_object foreign key (oid) references object(id) on update no action on delete cascade;
alter table measure drop constraint if exists fk_sensor;
alter table measure add constraint fk_sensor foreign key (sid) references sensor(id) on update no action on delete cascade;
alter table measure drop constraint if exists pk_measure;
alter table measure add constraint pk_measure primary key (oid,sid,timestamp);
create index if not exists measure_ot_idx ON measure (oid,timestamp);
create index if not exists measure_st_idx ON measure (sid,timestamp);
create index if not exists measure_os_idx ON measure (oid,sid);
create index if not exists measure_t_idx ON measure (timestamp);

* * *

Параметры настроек доступны в файле src/main/resources/application.properties:
- настройки подключения к базе данных: spring.jdbc.*
- размер пакета записей для пакетной заливки в базу: app.db.insert_batch_size
- размер пакета для пакетной выгрузки записей из базы: app.db.fetch_size

* * *

Сборку можно проводить при помощи скрипта gradlew в корне приложения:
./gradlew build
Запуск командой:
./gradlew run

В случае успешного запуска приложение соединится с базой данных, будет запущен 
встроенный Tomcat, слушающий localhost:8080. Управлять приложением можно при 
помощи REST-запросов.

* * *

TESTS

Загрузка записей в базу POST-запросом. Тело должно содержать валидный json - массив записей:
curl -d '[{"objectId": 0, "sensorId": 9, "time": 1584149642, "value": 111.0},{"objectId": 2, "sensorId": 9, "time": 1584149642, "value": 33.0}]' -H "Content-Type: application/json" -X POST http://localhost:8080/api/save

Загрузка в базу из файла data.json. Файл должен лежать в каталоге src/main/resource приложения  
(или в других каталогах classpath) до запуска приложения. 
Грузится все пакетами размера app.db.insert_batch_size 
(стандартный файл питоновской генерации в 2592002 записей грузится пару минут на моем lenovo e580). 
Загрузка вызывается запросом:
curl -d "data.json" -H "Content-Type: text/plain" -X POST http://localhost:8080/api/save/file

Получение логов датчика за указанный промежуток времени:
- Получение всех требуемых логов
curl -X GET 'http://localhost:8080/api/history?id=2&from=1584149642&to=1584149650'
- Выборка логов в отрезке [offset, offset+limit] записей 
curl -X GET 'http://localhost:8080/api/history?id=2&from=1584149642&to=1584149650&offset=10&limit=20'

Получение текущих (последних измерений датчиков, доступных транзакции) логов объекта:
curl -X GET 'http://localhost:8080/api/latest?id=2'

Получение усредненных по объектам текущих (последних измерений датчиков, доступных транзакции) 
значений измерений датчиков :
curl -X GET 'http://localhost:8080/api/avg'

* * *

Дополнения

Добавил логи с засечками времени, сброс в консоль и файл. Смотреть можно примерно так:
$tail -f $simple_scada/logs/scada_local.log | egrep -i 'scada'

* * *

Замечания

В задаче нарушена логика структуры данных: 
 если датчики уникальны, то не может быть их повтора в разных домах.
 Если датчики не уникальны, т.е. под sensor_id подразумевается их тип, то тогда накладывается ограничение:
 в каждом доме не более одного датчика подобного типа.

К сожалению не удалось настроить встроенную базу данных и тестовый профиль в целом, 
т.к. это мой первый опыт со spring boot. Так же не стал использовать JPA, т.к. длительное время с ним не работал. 

Не удалось малой кровью победить проблему разделителя ';' при создании функции заполнения в DDL.
Программный Populator делать не стал. 