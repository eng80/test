package scada.dto;

public class Record {
    private Integer objectId;
    private Integer sensorId;
    private Long time;
    private Double value;

    public Record(Integer objectId, Integer sensorId, Long time, Double value) {
        this.objectId = objectId;
        this.sensorId = sensorId;
        this.time = time;
        this.value = value;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getSensorId() {
        return sensorId;
    }

    public void setSensorId(Integer sensorId) {
        this.sensorId = sensorId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Record{" + "objectId=" + objectId + ", sensorId=" + sensorId + ", time=" + time + ", value=" + value + '}';
    }
    
}
