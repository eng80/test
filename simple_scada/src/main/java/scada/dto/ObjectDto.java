package scada.dto;

public class ObjectDto {
    private Integer objectId;
    private double avg;

    public ObjectDto(Integer objectId, double sensorAvg) {
        this.objectId = objectId;
        this.avg = sensorAvg;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public double getAvg() {
        return avg;
    }
}
