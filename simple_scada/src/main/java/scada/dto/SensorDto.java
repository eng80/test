package scada.dto;

public class SensorDto {
    private Integer sensorId;
    private Double value;

    public SensorDto(Integer sensorId, Double value) {
        this.sensorId = sensorId;
        this.value = value;
    }

    public Integer getSensorId() {
        return sensorId;
    }

    public Double getValue() {
        return value;
    }
    
    
}
