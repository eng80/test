package scada.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;
import scada.dto.*;

@Repository
public class ScadaDao {

    @Value("${app.db.fetch_size:50000}")
    private int fetch_size;
    private JdbcTemplate jdbcTemplate;
    private DataSource dataSource;
    private final static Logger LOG = LoggerFactory.getLogger(ScadaDao.class);

    @Autowired
    ScadaDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostConstruct
    public void init() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * Загрузка в рбд пакета записей
     */
    public int insertRecords1(List<Record> records) {
        int result = 0;
        if (CollectionUtils.isEmpty(records)) {
            return result;
        }
        String sql = "with new_object as "
                + "(insert into object (id) values (?) "
                + "on conflict (id) do update set id=EXCLUDED.id returning id), "
                + "new_sensor as (insert into sensor (id) values (?) "
                + "on conflict (id) do update set id=EXCLUDED.id returning id) "
                + "insert into measure (oid,sid,timestamp,value) "
                + "values ((select id from new_object), (select id from new_sensor), "
                + "?, ?);";

        StopWatch sw = new StopWatch();
        sw.start();
        int[] updateCounts = jdbcTemplate.batchUpdate(sql,
                new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int ind) throws SQLException {
                ps.setInt(1, records.get(ind).getObjectId());
                ps.setInt(2, records.get(ind).getSensorId());
                ps.setLong(3, records.get(ind).getTime());
                ps.setDouble(4, records.get(ind).getValue());
            }

            @Override
            public int getBatchSize() {
                return records.size();
            }
        });
        sw.stop();
        result = Arrays.stream(updateCounts).sum();
        LOG.debug("Insert records: size - {}, ms - {}", result, sw.getTotalTimeMillis());
        return result;
    }

    public int insertRecords(List<Record> records) {
        int result = 0;
        if (CollectionUtils.isEmpty(records)) {
            return result;
        }
        String sql;
        StopWatch sw = new StopWatch();
        sw.start();
        sql = "insert into object (id) values (?) on conflict do nothing";
        jdbcTemplate.batchUpdate(sql,
                new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int ind) throws SQLException {
                ps.setInt(1, records.get(ind).getObjectId());                
            }
            @Override
            public int getBatchSize() {
                return records.size();
            }
        });
        sql = "insert into sensor (id) values (?) on conflict do nothing";
        jdbcTemplate.batchUpdate(sql,
                new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int ind) throws SQLException {
                ps.setInt(1, records.get(ind).getSensorId());
            }

            @Override
            public int getBatchSize() {
                return records.size();
            }
        });
        sql = "insert into measure (oid,sid,timestamp,value) values (?,?,?,?) on conflict do nothing";
        int[] updateCounts = jdbcTemplate.batchUpdate(sql,
                new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int ind) throws SQLException {
                ps.setInt(1, records.get(ind).getObjectId());
                ps.setInt(2, records.get(ind).getSensorId());
                ps.setLong(3, records.get(ind).getTime());
                ps.setDouble(4, records.get(ind).getValue());
            }

            @Override
            public int getBatchSize() {
                return records.size();
            }
        });
        sw.stop();
        result = Arrays.stream(updateCounts).sum();
        LOG.debug("Insert records: size - {}, ms - {}", result, sw.getTotalTimeMillis());
        return result;
    }   
    
    
    /**
     * Получение показаний сенсора в заданный отрезок времени
     */
    public List<Record> getSensorLog(Integer sensorId, Long t0, Long t1, Integer offset, Integer limit) {
        final List<Record> result = new LinkedList<>();
        StringBuilder sql = new StringBuilder();
        sql.append("select m.oid,m.sid,m.timestamp,m.value")
                .append(" from measure m where m.sid = ?")
                .append(" and m.timestamp between ? and ?");
        sql = limit == null ? sql : sql.append(" limit ?");
        sql = offset == null ? sql : sql.append(" offset ?");
        StopWatch sw = new StopWatch();
        sw.start();
        jdbcTemplate.query(sql.toString(), ps -> {
            ps.setFetchSize(fetch_size);
            ps.setInt(1, sensorId);
            ps.setLong(2, t0);
            ps.setLong(3, t1);
            if (limit != null) {
                ps.setInt(4, limit);
            }
            if (offset != null) {
                ps.setInt(5, offset);
            }
        }, rs -> {
            while (rs.next()) {
                Record record = rs2Record(rs);
                result.add(record);
            }
            return null;
        });
        sw.stop();
        LOG.debug("Select sensor logs: size - {}, ms - {}", result.size(), sw.getTotalTimeMillis());
        return result;
    }

    /**
     * Получение текущих показаний счетчиков указанного объекта
     */
    public List<Record> getObjectLog(Integer objectId) {
        final List<Record> result = new LinkedList<>();
        StringBuilder sql = new StringBuilder();
        sql.append("select m.oid,m.sid,m.timestamp,m.value from measure m where m.oid=? and m.timestamp =")
                .append(" (select max(m1.timestamp) from measure m1 where m1.oid=m.oid and m1.sid=m.sid)");
        StopWatch sw = new StopWatch();
        sw.start();
        jdbcTemplate.query(sql.toString(), ps -> {
            ps.setFetchSize(fetch_size);
            ps.setInt(1, objectId);
        }, rs -> {
            while (rs.next()) {
                Record record = rs2Record(rs);
                result.add(record);
            }
            return null;
        });
        sw.stop();
        LOG.debug("Select object logs: size - {}, ms - {}", result.size(), sw.getTotalTimeMillis());
        return result;
    }

    /**
     * Усредненные по объектам измерения в текущий момент времени
     */
    public List<ObjectDto> getObjectAvg() {
        final List<ObjectDto> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        sql.append("select m.oid, avg(m.value) as mean from measure m where m.timestamp ="
                + " (select max(m1.timestamp) from measure m1 where m1.oid = m.oid and m1.sid = m.sid)"
                + " group by m.oid");
        StopWatch sw = new StopWatch();
        sw.start();
        jdbcTemplate.query(sql.toString(), ps -> {
            ps.setFetchSize(fetch_size);
        },
                rs -> {
                    while (rs.next()) {
                        ObjectDto objectDto = new ObjectDto(rs.getInt("oid"), rs.getDouble("mean"));
                        result.add(objectDto);
                    }
                    return null;
                });
        sw.stop();
        LOG.debug("Get object avgs: size - {}, ms - {}", result.size(), sw.getTotalTimeMillis());
        return result;
    }

    private Record rs2Record(ResultSet rs) throws SQLException {
        return new Record(rs.getInt("oid"), rs.getInt("sid"), rs.getLong("timestamp"), rs.getDouble("value"));
    }
}
