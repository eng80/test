package scada;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.SpringApplication;

//@ComponentScan("scada")
@SpringBootApplication
public class App {

    public static void main(String[] args) {
           SpringApplication.run(App.class, args);
    }
}