package scada.controller;

import java.io.Reader;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import scada.dto.*;
import scada.service.*;

@RestController
public class ScadaController {
    private ScadaService scadaService;
    
    @Autowired
    public ScadaController(ScadaService scadaService) {
        this.scadaService = scadaService;
    }
    
    @RequestMapping(value = "/api/save/file", method = POST)
    public String saveFromFile(@RequestBody String fileName) {
        int size = scadaService.saveFromFile(fileName);
        return "Num of inserted rows is: " + size;
    }
    
    @RequestMapping(value = "/api/save", method = POST)
    public String save(HttpServletRequest req, HttpServletResponse resp) {
        int size = 0;
        try(Reader reader = req.getReader()) {
            size = scadaService.save(reader);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return "Num of inserted rows is: " + size;
    }
    
    ///api/history?id=1&from=1565654400&to=1565827200
    ///api/history?id=1&from=1565654400&to=1565827200&offset=10&limit=20
    @RequestMapping(value = "/api/history", method = GET) 
    public List<Record> getHistory(
            @RequestParam(name="id",required = true) Integer sid, 
            @RequestParam(name="from",required = true) Long t0, 
            @RequestParam(name="to",required = true) Long t1,
            @RequestParam(name="offset",required = false) Integer offset, 
            @RequestParam(name="limit",required = false) Integer limit) {
        return scadaService.getSensorLog(sid,t0,t1,offset,limit);
    }
    
    ///api/latest?id=1
    @RequestMapping(value = "/api/latest", method = GET)
    public List<Record> getObjectLog(@RequestParam(name="id",required = true) Integer oid) {
        return scadaService.getObjectLog(oid);
    }
    
    ///api/avg
    @RequestMapping(value = "/api/avg", method = GET)
    public List<ObjectDto> getAvg() {
        return scadaService.getObjectAvg();
    }    
}