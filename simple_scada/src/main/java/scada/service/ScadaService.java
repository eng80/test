package scada.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import scada.dto.*;
import scada.dao.*;

@Service
public class ScadaService {
    @Value("${app.db.insert_batch_size:100000}")
    private static int insert_batch_size = 100000;
    private ScadaDao scadaDao;
    
    @Autowired
    ScadaService(ScadaDao scadaDao) {
        this.scadaDao = scadaDao;
    }
    
    public List<Record> getSensorLog(Integer sensorId, Long t0, Long t1, Integer offset, Integer limit) {
        return scadaDao.getSensorLog(sensorId,t0,t1,offset,limit);
    }
    
    public List<Record> getObjectLog(Integer objectId) {
        return scadaDao.getObjectLog(objectId);
    }
    
    public List<ObjectDto> getObjectAvg() {
        return scadaDao.getObjectAvg();
    }    
    
    public int save(Reader reader) {
        return json2RecordExecutor(reader,insert_batch_size,scadaDao::insertRecords);
    }
    
    public int saveFromFile(String fileName) {
        int result = 0;
        try(InputStreamReader reader = 
                new InputStreamReader(new FileInputStream(Paths.get(ScadaService.class.getClassLoader()
                .getResource(fileName).toURI()).toFile()), StandardCharsets.UTF_8)) {
                result =  save(reader);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    
    private int json2RecordExecutor(Reader isr, int batchSize,Function<? super List<Record>,Integer> batchExecutor) {
        int result = 0;
        try (JsonReader jsonReader = new JsonReader(isr)) {
            Gson gson = new GsonBuilder().create();
            jsonReader.beginArray();
            int count = 0;
            List<Record> batch = new ArrayList<>();
            int batchNo = 1;
            while (jsonReader.hasNext()) {
                if(++count > batchSize) {
                    result += batchExecutor.apply(batch);
                    batch.clear();
                    count = 0;
                }           
                Record record = gson.fromJson(jsonReader, Record.class);
                batch.add(record);
            }
            result += batchExecutor.apply(batch);
            jsonReader.endArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }    
    
}
