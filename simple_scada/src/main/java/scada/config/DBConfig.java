package scada.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class DBConfig {

    @Bean
    public DataSource postgresDataSource(
            @Value("${spring.jdbc.driver:org.postgresql.Driver}") String jdbcDriver,
            @Value("${spring.jdbc.url:jdbc:postgresql://localhost:5432/scada}") String jdbcUrl,
            @Value("${spring.jdbc.username:scada_manager}") String jdbcUsername,
            @Value("${spring.jdbc.password:scada}") String jdbcPassword
    ) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(jdbcDriver);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(jdbcUsername);
        dataSource.setPassword(jdbcPassword);
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@Autowired DataSource postgresDataSource) {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(postgresDataSource);
        return transactionManager;
    }
}
